
var app = require('express')();
var http = require('http').Server(app);
var request = require('request');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var users =  {};
var clients = {};

app.get('/', function(req, res){	
  data = {
  	type: 'notification',
  	id: req.query.id
  }
  setTimeout(function(){
  	res.send(JSON.stringify(data));	
  }, 5000)
  
});

http.listen(3000, function(){
	console.log('Server running...')
})
