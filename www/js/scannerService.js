
var scannerService = function(){

  var scan = function(){
    ionic.Platform.ready(function(){
        scanner.startScanning(function(){}, function(result){console.log(result)});
    })
  }
  var scanPartial = function(){
    ionic.Platform.ready(function(){
        scanner.startScanning(50,50,200,100);
    })
  }
  var scanImage = function(){
    function init(Scanner, codes){
      Scanner.MWBregisterCode(codes.MWB_CODE_MASK_QR, "lperez@hexacta.com", "1EE9A87A6D0E47FF8FB84B94B36F87974FC951D2D567101E2172F3B14E36BBDE");
    }
    function cbk(result){
      console.log('result: ', result);
    }
    ionic.Platform.ready(function(){
        scanner.scanImage(init, cbk,"img/barcode.png");
    })
  }
  var stop = function(){
    scanner.stopScanner();
  }

  return {
    scan: scan,
    stop: stop,
    scanPartial: scanPartial,
    scanImage: scanImage
  };
}
angular.module('starter').factory('scannerService', scannerService);