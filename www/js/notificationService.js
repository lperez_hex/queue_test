/*
	Modulo de notificaciones ------->

	1. cuando se agrega una notificacion se guarda en localStorage
	2. cuando se procesa la cola, se ejecuta la peticion al servidor, si es exitosa se elimina de localstorage y se emite el evento notification.sent
	3. si la peticion devuelve error, se incrementa la cantidad de intentos (no se borra de la cola). si el numero de intentos llega al limite se emite el evento notification-error
	4. En otra parte de la aplicacion (o aqui imsmo), se escucha a los eventos online y offline de cordovaNetwork para procesar o pausar la cola

*/
var notificationService = function($rootScope, $http, $queue){
	
	var limit = 3;
	var notifications = [];
	var processing = false;
	var queueLength = 0;
	window.localStorage.setItem('notifications', '[]')

	var processNotification = function(data){

		var notification = getNotification(data.id);
		$http.get('http://10.120.2.82:3000?id='+data.id).then(function(response){
			deleteNotification(data.id)//elimina notificacion de local storage
			$rootScope.$broadcast('notification-sent', data.id)
			queueLength -= 1;
			if(queueLength==0){
				queueComplete();
			}

		}, function(err){
			notification.attempts += 1;
			if(notification.attempts > limit){
				deleteNotification(data.id)
				$rootScope.$broadcast('notification-error', data.id)	
				//eliminar de la cola
			}else{
				console.log('error en notificacion '+data.id+' intentos: '+data.attempts);
				updateNotification(notification.id, notification);	//actualiza la cotificacion en localstorage con un intento mas
			}
			queueLength -= 1;
			if(queueLength==0){
				queueComplete();
			}
		})
	}
	var queueComplete = function(){
		processing = false;
		getNotifications();
		if(notifications.length){
			processQueue();
		}
	}
	var isProcessing = function(){
		return processing;
	}

	var queue = $queue.queue(processNotification, {paused:false});

	var processQueue = function(){
		if(!processing){
			processing = true;
			queue.clear();
			getNotifications(); //levanta las notificaciones de localstorage
			queueLength =  notifications.length;
			queue.addEach(notifications);
			queue.start();
		}
	}
	var pauseQueue = function(){
		console.log('cola en pausa');
		queue.pause();
	}
	var addItem = function(data){
		data.id = guid();
		console.log('Notificacion '+data.id+' en cola');
		data.attempts = 0;
		getNotifications();
		notifications.push(data);
		saveNotifications();// guarda notificacion el localstorage
		
	}
	var saveNotifications = function(){
		window.localStorage.setItem('notifications',JSON.stringify(notifications));
	}
	var getNotifications = function(){
		notifications = JSON.parse( window.localStorage.getItem('notifications'));
		return notifications;
	}
	var getNotification = function(id){
		getNotifications();
		angular.forEach(notifications, function(value, key) {
			if (value.id == id){
				res = notifications[key]	
			}
		});
		return res;

	}
	var deleteNotification = function(id){
		getNotifications();
		angular.forEach(notifications, function(value, key) {
			if (value.id == id){
				notifications.splice(key, 1)	
			}
		});
		saveNotifications();
	}
	var updateNotification = function(id, data){
		getNotifications();
		angular.forEach(notifications, function(value, key) {
			if (value.id == id){
				notifications[key] = data;	
			}
		});
		saveNotifications();
	}
	var guid = function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	    s4() + '-' + s4() + s4() + s4();
	}
	return{
		addItem: addItem,
		processQueue: processQueue,
		pauseQueue: pauseQueue,
		getNotifications: getNotifications,
		isProcessing: isProcessing
	}

}
notificationService.$inject = ['$rootScope', '$http', '$queue']
angular.module('starter').factory('notificationService', notificationService);