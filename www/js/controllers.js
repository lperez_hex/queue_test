angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope,$rootScope, $queue, notificationService,$cordovaNetwork) {
  
  var updateList = function(){
    $scope.notifications = notificationService.getNotifications();  
  }

  $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
    setTimeout(function(){
      if($cordovaNetwork.isOnline()){
        console.log('online');
        notificationService.processQueue();     
      }
    }, 5000)
    
  })
  $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
    console.log('offline');
    notificationService.pauseQueue();
  })
  $scope.$on('notification-sent', function(e, response){
    console.log('Notificaccion '+response+' enviada');
    updateList();
  })
  $scope.$on('notification-error', function(e, response){
    console.log('error de conexion al servidor ',response);
    updateList();
  })
  
  $scope.newNotification = function(){
    notificationService.addItem({data: 'new notification'});
    updateList();
    if($cordovaNetwork.isOnline()){
      notificationService.processQueue();   
    }
  }
  notificationService.addItem({data: '1n'});
  notificationService.addItem({data: '2n'});
  notificationService.addItem({data: '3n'});
  updateList();

  setTimeout(function(){
    if($cordovaNetwork.isOnline()){
       //notificationService.processQueue();   
    }
  }, 3000)

})

.controller('FileCtrl', function($scope, fileService) {

  var url = 'http://support.amd.com/TechDocs/Bolton_D2-D2H-D3-D4_Databook.pdf';
  var localFile = '';

  $scope.downloaded = false;

  $scope.download = function(){
    fileService.downloadFile(url).then(function(data, prog){
      console.log(prog);
      console.log(data);
      localFile = data.name;
      $scope.downloaded = true;
    }, function(e){
      console.log(e);
    });
  }

  $scope.open = function(){
    fileService.openFile(localFile, 'app').then(function(data){
      console.log(data);
    }, function(err){
      console.log(err);
    });
  }

  $scope.share = function(){
    fileService.share(localFile).then(function(data){
      console.log(data);
    }, function(err){
      console.log(err);
    })  
  }

  $scope.inAppBrowser = function(){
    console.log(url)
    fileService.openFile(url, 'browser').then(function(data){
      console.log(data);
    }, function(err){
      console.log(err);
    });  
  }

})

.controller('DropboxCtrl', function($scope, fileService, $cordovaSocialSharing) {

  $scope.share = function(){
    console.log('share');
     
  }

})
.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, scannerService) {
  $scope.scanning = false;
  $scope.scan = function(){
      scannerService.scan();
  }
  $scope.scanPartial = function(){
      $scope.scanning = true;
      scannerService.scanPartial();
  }
  $scope.scanImage = function(){
      scannerService.scanImage();
  }
  $scope.stopScanning = function(){
      scannerService.stop();
  }
});
