var fileService = function($cordovaFile, $cordovaFileTransfer, $cordovaFileOpener2, $cordovaSocialSharing, $cordovaInAppBrowser){
	
	var path = cordova.file.externalDataDirectory;
	
	var downloadFile = function(url){
		var filePath = path + 'descarga1.pdf';
		var options = {};
		return $cordovaFileTransfer.download(url, filePath, options, true);
	}

	var openFile = function(file, openWith ,mimeType){
		var openWith = openWith || 'browser';
		var filePath = path  + file;
		var mimeType = mimeType || 'application/pdf';

		if(openWith == 'browser'){
			return $cordovaInAppBrowser.open(file, '_system', {'location':'yes'})
		}
		if(openWith == 'app'){
			return $cordovaFileOpener2.open( filePath, mimeType);	
		}
		
    }
    var share = function(file){
    	return $cordovaSocialSharing.share(null, null, file)	
    }
    return {
    	downloadFile: downloadFile,
    	openFile: openFile,
    	share: share
    }
	
}

fileService.$inject = ['$cordovaFile', '$cordovaFileTransfer', '$cordovaFileOpener2', '$cordovaSocialSharing', '$cordovaInAppBrowser'];
angular.module('starter').factory('fileService', fileService);