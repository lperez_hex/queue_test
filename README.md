# README #
## Concepto de servicio de cola de notificaciones ##


```
#!bash

npm install #para instalar dependencias
bower install #para dependencias del frontend
```


servicio en www/js/notificationService.js
prueba en www/js/controllers.js dashCtrl 

/*
	Modulo de notificaciones ------->

	1. cuando se agrega una notificacion se guarda en localStorage
	2. cuando se procesa la cola, se ejecuta la peticion al servidor, si es exitosa se elimina de localstorage y se emite el evento notification.sent
	3. si la peticion devuelve error, se incrementa la cantidad de intentos (no se borra de la cola). si el numero de intentos llega al limite se emite el evento notification-error
	4. En otra parte de la aplicacion (o aqui imsmo), se escucha a los eventos online y offline de cordovaNetwork para procesar o pausar la cola

*/
## server de prueba en server/server.js ##

```
#!bash

node server.js
```